var generalService = require('../../../../library/general.service');

// parte privada
var products = [
    {
        name: 'Galletas Cuétara',
        id: 1311
    },
    {
        name: 'Caramelos de menta',
        id: 1312
    },
    {
        name: 'Leche Pascual 1 litro',
        id: 1313
    }
];

// funciones síncronas
function getProductById(id) {
    for (let i = 0; i < products.length; i++) {
        if (id === products[i].id) {
            return products[i];
        }
    }
}
function itemExist(nameProperty, value) {
    for (let i = 0; i < products.length; i++) {
        if (products[i][nameProperty] === value) {
            return true;
        }
    }
    return false;
}
function updateEntireElement(element) {
    for (let i = 0; i < products.length; i++) {
        if (element.id === products[i].id) {
            products[i] = element;
            return;
        }
    }
}
function updatePartialProduct(element) {
    for (let i = 0; i < products.length; i++) {
        if (element.id === products[i].id) {
            for (const key in element) {
                products[i][key] = element[key];
            }
            return;
        }
    }
}
function deleteElement(element) {
    for (let i = 0; i < products.length; i++) {
        if (element.id === products[i].id) {
            products.splice(i, 1);
            return;
        }
    }
}

// parte pública
module.exports = {
    getOneProduct(segmentUrl, req, res) {
        const id = segmentUrl[segmentUrl.length - 1];
        if (id === '' | id === '/') {
            // respuesta
            res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
            var error = {
                error: {
                    codigo: "001",
                    descripcion: "Bad request",
                    detalles: [
                        "Error: campo 'id' inválido"
                    ]
                }
            }
            res.end(JSON.stringify(error));
        } // compruebo que exista el registro que quiero que sea actualizado
        else if (!itemExist('id', Number(id))) {
            // respuesta
            res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
            var error = {
                error: {
                    codigo: "001",
                    descripcion: "Bad request",
                    detalles: [
                        "Error de negocio",
                        `No existe el registro que quieres actuializar con el campo 'id': '${data.id}'`
                    ]
                }
            }
            res.end(JSON.stringify(error));
        } // actualizo el nuevo registro entero
        else {
            var product = getProductById(Number(id));
            // respuesta
            res.writeHead(201, { 'Content-Type': 'application/json; charset=UTF-8' });
            res.end(JSON.stringify(product));
        }
    },
    getAllProducts(req, res) {
        res.writeHead(200, { 'Content-Type': 'application/json; charset=UTF-8' });
        res.end(JSON.stringify(products));
    },
    // ejemplo javaScript "asíncrono" con PROMISE: bloque "THEN" y "CATH"
    setProduct(req, res) {
        generalService.getBody(req)
            // si me llegan los datos
            .then(data => {
                // NEGOCIO: campo "name" obligatorio
                if (!data.name) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                "Falta el campo 'name'"
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // compruebo que no esté repetido el registro
                else if (itemExist('name', data.name)) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                `Ya existe un registro con el campo 'name': '${data.name}'`
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // salvo el nuevo registro
                else {
                    products.push(data);
                    // respuesta
                    res.writeHead(201, { 'Content-Type': 'application/json; charset=UTF-8' });
                    res.end(JSON.stringify(products));
                }
            })
            // en caso de no llegarme los datos
            .catch(err => {
                console.error('$$$ products.service setProduct() err: ', err);
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Faltan los datos a guardar",
                            "Has realizado una petición vacía"
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            });
    },
    // ejemplo javaScript "asíncrono" con "ASYNC" y "AWAIT": bloque "TRY" y "CATH"
    async updateAllProduct(req, res) {
        // petición POST con BODY
        try {
            var data = await generalService.getBody(req);
            // si me llegan los datos
            // NEGOCIO: campo "name" obligatorio
            if (!data.name) {
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Error de negocio",
                            "Falta el campo 'name'"
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            } // compruebo que tenga ID el elemento
            else if (!data.id) {
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Error: falta el campo 'id'"
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            } // compruebo que exista el registro que quiero que sea actualizado
            else if (!itemExist('id', data.id)) {
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Error de negocio",
                            `No existe el registro que quieres actuializar con el campo 'id': '${data.id}'`
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            } // actualizo el nuevo registro entero
            else {
                updateEntireElement(data);
                // respuesta
                res.writeHead(201, { 'Content-Type': 'application/json; charset=UTF-8' });
                res.end(JSON.stringify(products));
            }
        }
        catch (err) {
            console.err('$$$ products.service updateAllProduct() err: ', err);
            // respuesta
            res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
            var error = {
                error: {
                    codigo: "001",
                    descripcion: "Bad request",
                    detalles: [
                        "Faltan los datos a guardar",
                        "Has realizado una petición vacía"
                    ]
                }
            }
            res.end(JSON.stringify(error));
        }
    },
    updatePartialProduct(req, res) {
        generalService.getBody(req)
            // si me llegan los datos
            .then(data => {
                // NEGOCIO: campo "name" obligatorio
                if (!data.name) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                "Falta el campo 'name'"
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // compruebo que recibo el campo ID
                else if (!data.id) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error: falta el campo 'id'"
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                }
                // compruebo que SI esté el registro para ser actualizado
                else if (!itemExist('id', data.id)) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                `No existe el registro que quieres actuializar con el campo 'id': '${data.id}'`
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // salvo el nuevo registro
                else {
                    updatePartialProduct(data);
                    // respuesta
                    res.writeHead(201, { 'Content-Type': 'application/json; charset=UTF-8' });
                    res.end(JSON.stringify(products));
                }
            })
            // en caso de no llegarme los datos
            .catch(err => {
                console.error('$$$ products.service updatePartialProduct() err: ', err);
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Faltan los datos a guardar",
                            "Has realizado una petición vacía"
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            });
    },
    deleteProduct(req, res) {
        generalService.getBody(req)
            // si me llegan los datos
            .then(data => {
                // campo "id" obligatorio para borrar un elemento
                if (!data.id) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                "Falta el campo 'id'"
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // compruebo que el registro existe para ser borrado
                else if (!itemExist('id', data.id)) {
                    // respuesta
                    res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                    var error = {
                        error: {
                            codigo: "001",
                            descripcion: "Bad request",
                            detalles: [
                                "Error de negocio",
                                `No existe el registro que quieres borrar con el campo 'id': '${data.id}'`
                            ]
                        }
                    }
                    res.end(JSON.stringify(error));
                } // salvo el nuevo registro
                else {
                    deleteElement(data);
                    // respuesta
                    res.writeHead(201, { 'Content-Type': 'application/json; charset=UTF-8' });
                    res.end(JSON.stringify(products));
                }
            })
            // en caso de no llegarme los datos
            .catch(err => {
                console.error('### products.service deleteProduct() err: ', err);
                // respuesta
                res.writeHead(400, { 'Content-Type': 'application/json; charset=UTF-8' });
                var error = {
                    error: {
                        codigo: "001",
                        descripcion: "Bad request",
                        detalles: [
                            "Faltan los datos a guardar",
                            "Has realizado una petición vacía"
                        ]
                    }
                }
                res.end(JSON.stringify(error));
            });
    }
}