var productsController = require('./controllers/products/products.controller');

module.exports = {
    config: function (segmentUrl, req, res) {
        // ### API TORNILLOS ### => /api/dato-unico/v1.0/productos
        if (segmentUrl[2] === 'dato-unico'
                 && segmentUrl[3] === 'v1.0'
                 && segmentUrl[4] === 'productos')
        {
          productsController.getProducts(segmentUrl, req, res);

        } else {
            res.writeHead(404, { 'Content-Type': 'application/json; charset=UTF-8' });
            var errorResponse = {
                error: {
                    codigo: 'NOT_FOUND',
                    descripcion: 'micro-service endpoint no encontrado'
                }
            };
            res.end(JSON.stringify(errorResponse));
        }
    }
}
