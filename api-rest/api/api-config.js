var storeController = require('./controllers/stores.controller');
var employeesController = require('./controllers/employees.controller');

module.exports = {
    config: function (segmentUrl, req, res) {
        // ### API TIENDAS ### => /api/dato-unico/v1.0/tiendas
        if (segmentUrl[2] === 'dato-unico'
            && segmentUrl[3] === 'v1.0'
            && segmentUrl[4] === 'tiendas'
            && req.method === 'GET')
        {
            storeController.getStores(req, res);

        } // ### API EMPLEADOS ### => /api/dato-unico/v1.0/empleados
        else if (segmentUrl[2] === 'dato-unico'
                 && segmentUrl[3] === 'v1.0'
                 && segmentUrl[4] === 'empleados'
                 && req.method === 'GET')
        {
          employeesController.getEmployees(req, res);

        } else {
            res.writeHead(404, { 'Content-Type': 'application/json; charset=UTF-8' });
            var errorResponse = {
                error: {
                    codigo: 'NOT_FOUND',
                    descripcion: 'api-rest Endpoint no encontrado'
                }
            };
            res.end(JSON.stringify(errorResponse));
        }
    }
}
