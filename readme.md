# CONCEPTOS API

## RAMA: branch12

#### Separación del API Rest de "productos" backend de estáticos con el de API

1. vamos a separar el backend de APIs con respecto al del API de productos
    *  esto nos permitirá tener una mayor escalabilidad para esta API en concreto

2. el backend de API Rest de "products", está configurado para pasarle por parámetro, el número de puerto al que debe de levantarse
    *  con esta configuración del puerto pasado por parámetros, podemos levantar tantas instancias como queramos
    *  PORT=2020 node server
    *  PORT=2021 node server
    *  ahora sólo faltaría poner por delante un "balanceador de carga"
