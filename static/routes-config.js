// servicios de NODEJS
var fs = require('fs');
// archivos de la aplicación
var configServer = require('./config-server');

module.exports = {
    config: function (req, res) {
        // carpeta donde se encuentran los archivos estáticos
        var filePath = configServer.filePathHtml;
        switch (req.url) {
            case '/':
            case '/index.html':
                filePath += '/index.html';
                break;
            case '/tiendas':
            case '/tiendas.html':
                filePath += '/tiendas.html';
                break;
            case '/productos':
            case '/productos.html':
                filePath += '/productos.html';
                break;
            case '/empleados':
            case '/empleados.html':
                filePath += '/empleados.html';
                break;
            case '/notfound':
            case '/notfound.html':
                filePath += '/notfound.html';
                break;
            case '/favicon.ico':
                filePath += '/favicon.ico';
                break;
            default:
                // ### opción 1 not found: Devolvemos un texto a la misma URL
                // filePath += '/notfound.html';
                // ### opción 2 not found: Obligamos a un redireccionamiento
                res.statusCode = 301;
                res.setHeader('Location', '/notfound');
                // res.writeHead(301, { "Location": '/notfound'});
                return res.end();
        }
        console.log(' @ filePath: ', filePath);

        fs.readFile(filePath, function (err, data) {
            // control de errores
            if (!err) {
                // resonse SUCCESS
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.write(data);
                res.end();
            } else {
                // NOT FOUND
                console.error('&&& routes-config config() fs.readFile err: ', err)
                res.writeHead(404, { 'Content-Type': 'text/html' });
                res.end("404 Not Found");
            }
        });
    }
}
