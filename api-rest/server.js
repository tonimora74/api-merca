// bootstrapping file
var http = require('http');
var configServer = require('./config-server');
var apiConfig = require('./api/api-config');
var library = require('../library/general.service');

// 00- configuración del servidor
const hostname = configServer.hostname;
const port = configServer.port;

const server = http.createServer(function (req, res) {
  console.log('### req pathname: ', req.url);
  console.log(' @ req method: ', req.method);
  
  // separar las rutas API de las peticiones de archivo
  var segmentUrl = library.getSegmentFromUrl(req);
  console.log(' @ segmentUrl: ', segmentUrl);
  if(segmentUrl[1] === 'api') {
    // configuración del API
    apiConfig.config(segmentUrl, req, res);

  } else {
    // error
    console.error('&&& API server err: url API no encontrada');
    // devolución de error
    res.writeHead(404, { 'Content-Type': 'application/json; charset=UTF-8' });
    var errorResponse = {
        error: {
            codigo: '404',
            descripcion: [
              'Endpoint no encontrado',
              'Api-rest err: url mal redirigida',
              'Póngase en contacto con el administrador del sistema'
            ]
        }
    };
  }
});

server.listen(port, hostname, () => {
  console.log(`### Server running at http://${hostname}:${port}/`);
});
