var productServices = require('./products.service');

module.exports = {
    getProducts: function (segmentUrl, req, res) {
      // /api/dato-unico/v1.0/productos/1312
      if(segmentUrl.length === 6 && req.method === 'GET') {
        productServices.getOneProduct(segmentUrl, req, res);
      } else {
        // /api/dato-unico/v1.0/productos
        switch (req.method) {
          // obtener
          case 'GET':
            productServices.getAllProducts(req, res);
            break;
          // guardar un nuevo registro
          case 'POST':
            productServices.setProduct(req, res);
            break;
          // actualiza completamente un registro
          case 'PUT':
            productServices.updateAllProduct(req, res);
            break;
          // actualiza parcialmente un registro
          case 'PATCH':
            productServices.updatePartialProduct(req, res);
            break;
          // borra un registro
          case 'DELETE':
            productServices.deleteProduct(req, res);
            break;
          // cualquier otro "method" (verbo), al no estar implementado, devolverá un error
          default:
            res.writeHead(405, {'Content-Type': 'application/json; charset=UTF-8'});
            var error = {
              error: {
                codigo: "001",
                descripcion: "El método usado no estás permitido",
                detalles: [
                  `Has usado el método: '${req.method}'`,
                  "Ese método no está implementado"
                ]
              }
            }
            res.end(JSON.stringify(error));
        }
      }
    }
}
