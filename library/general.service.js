var url = require('url');

module.exports = {
    getSegmentFromUrl: function(req) {
        var segments = req.url.split('/');
        if (segments[segments.length - 1] === '') {
            segments.pop();
        }
        return segments;
    },

    // función asíncrona
    getBody: function(req) {
        return new Promise(function (resolve, reject) {
            let body = [];
            req.on('error', (err) => {
                console.error('$$$ products.service getBody()', err);
            }).on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                if (body.length > 0) {
                    resolve(JSON.parse(Buffer.concat(body).toString()));
                } else {
                    reject(false);
                }
            });
        });
    }
}
