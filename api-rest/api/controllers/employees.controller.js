// parte privada
var employees = [
    {
      name: 'Fernando García',
      dni: '45634562P'
    },
    {
      name: 'Andrés Sabina',
      dni: '45924514M'
    },
    {
      name: 'José Castro',
      dni: '45294563L'
    }
];

// parte pública
module.exports = {
    
    getEmployees: function (req, res) {
      if(req.method === 'GET') {
        res.writeHead(200, {'Content-Type': 'application/json; charset=UTF-8'});
        res.end(JSON.stringify(employees));
      } else {
        res.writeHead(405, {'Content-Type': 'application/json; charset=UTF-8'});
        var error = {
          error: {
            codigo: "001",
            descripcion: "El método usado no estás permitido",
            detalles: [
              "Has usado el método: " + req.method,
              "Debes usar obligatoriamente el método 'GET'"
            ]
          }
        }
        return res.end(JSON.stringify(error));
      }
    }
}