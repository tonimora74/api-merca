// bootstrapping file
// servicios de NODEJS
var http = require('http');
// librería externa
var library = require('../library/general.service');
// archivos de la aplicación
var configServer = require('./config-server');
var routesConfig = require('./routes-config');

// 00- configuración del servidor
const hostname = configServer.hostname;
const port = configServer.port;

const server = http.createServer(function (req, res) {
  console.log('### req pathname: ', req.url);
  console.log(' @ req method: ', req.method);
  
  // separar las rutas API de las peticiones de archivo
  var segmentUrl = library.getSegmentFromUrl(req);
  console.log(' @ segmentUrl: ', segmentUrl);
  if(segmentUrl[1] === 'api') {
    // error
    console.error('&&& static server err: url API mal redirigida');
    // devolución de error
    res.writeHead(404, { 'Content-Type': 'application/json; charset=UTF-8' });
    var errorResponse = {
        error: {
            codigo: '404',
            descripcion: [
              'Endpoint no encontrado',
              'Static server err: url API mal redirigida',
              'Póngase en contacto con el administrador del sistema'
            ]
        }
    };
    res.end(JSON.stringify(errorResponse));

  } else {
    // configuración de rutas
    routesConfig.config(req, res);
  }
});

server.listen(port, hostname, () => {
  console.log(`### Server running at http://${hostname}:${port}/`);
});
