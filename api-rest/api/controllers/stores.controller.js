// parte privada
var stores = [
    {
      id: 1,
      name: 'Valencia',
      workersNumber: 20
    },
    {
      id: 2,
      name: 'Aldaia',
      workersNumber: 10
    },
    {
      id: 3,
      name: 'Alaquas',
      workersNumber: 5
    }
];

// parte pública
module.exports = {
    
    getStores: function (req, res) {
      if(req.method === 'GET') {
        res.writeHead(200, {'Content-Type': 'application/json; charset=UTF-8'});
        res.end(JSON.stringify(stores));
      } else {
        res.writeHead(405, {'Content-Type': 'application/json; charset=UTF-8'});
        var error = {
          error: {
            codigo: "001",
            descripcion: "El método usado no estás permitido",
            detalles: [
              "Has usado el método: " + req.method,
              "Debes usar obligatoriamente el método 'GET'"
            ]
          }
        }
        return res.end(JSON.stringify(error));
      }
    }
}